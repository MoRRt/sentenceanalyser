﻿namespace SentenceAnalyser.Models
{
    public class DataInfo
    {
        public int Length { get; set; }
        public int Count { get; set; }
        public string[] Words { get; set; }
    }
}