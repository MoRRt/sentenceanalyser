﻿using System.Collections.Generic;

namespace SentenceAnalyser.Models
{
    public class AnalyzerResponse
    {
        public List<DataInfo> Result { get; set; }

        public AnalyzerResponse()
        {
            Result = new List<DataInfo>();
        }
    }
}