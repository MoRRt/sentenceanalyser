﻿using Microsoft.Extensions.Logging;
using SentenceAnalyser.Utils.Logger;

namespace SentenceAnalyser.Utils.Extensions
{
    public static class FileLoggerExtension
    {
        public static ILoggerFactory AddFile(this ILoggerFactory factory,
            string filePath)
        {
            factory.AddProvider(new FileLoggerProvider(filePath));
            return factory;
        }
    }
}