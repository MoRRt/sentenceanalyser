﻿using SentenceAnalyser.Models;

namespace SentenceAnalyser.Services.Abstracts
{
    public interface IServiceAnalyzer
    {
        AnalyzerResponse DataProcessing(AnalyzerRequest data);
        bool ValidateSentence(string sentence);
    }
}