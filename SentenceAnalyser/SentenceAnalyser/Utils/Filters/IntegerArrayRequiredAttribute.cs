﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SentenceAnalyser.Utils.Filters
{
    public class IntegerArrayRequiredAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int[] array = value as int[];

            if (array == null || array.Length==0)
            {
                return new ValidationResult(ErrorMessage);
            }
            return ValidationResult.Success;
        }
    }
}