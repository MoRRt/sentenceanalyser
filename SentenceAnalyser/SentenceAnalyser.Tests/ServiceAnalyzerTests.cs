﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SentenceAnalyser.Models;
using SentenceAnalyser.Services;

namespace SentenceAnalyser.Tests
{
    [TestClass]
    public class ServiceAnalyzerTests
    {
        private const string SeparatedRegularExpressionTest = @"[-.?!)(;,]";

        private const string InvalidSentence =
            "To package an application to run in Docker, you write a small script called a Dockerfile that~_* automates all+= the steps for deploying the app&.";
        private const string ValidSentence =
                "To package an application to run in Docker, you write a small script called a Dockerfile that automates all the steps for deploying the app.";
        private static readonly int[] LengthsTest = {5, 8, 2, 6, 7};

        public static ServiceAnalyzer ServiceAnalyser { get; set; }

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ServiceAnalyser = new ServiceAnalyzer();
        }

        [TestMethod]
        public void DataProcessing_CanReturnCorrectWords()
        {
            AnalyzerRequest testRequest = new AnalyzerRequest {Sentence = ValidSentence, Lengths = LengthsTest };
            StringBuilder sbResult = new StringBuilder();

            var response = ServiceAnalyser.DataProcessing(testRequest);
           
            IEnumerable<DataInfo> dataList = response.Result.Where(info => info.Count != 0);
            foreach (var info in dataList) sbResult.AppendJoin("",info.Words);
            Assert.IsTrue(!Regex.IsMatch(sbResult.ToString(),SeparatedRegularExpressionTest));
        }

        [TestMethod]
        public void ValidateSentence_CanIdentifyInvalidSentence()
        {
            var result = ServiceAnalyser.ValidateSentence(InvalidSentence);

            Assert.IsFalse(result);
        }

        #region Exceptions
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void DataProcessing_AnalyzerRequestIsEmpty_ThrowsException()
        {
            ServiceAnalyser.DataProcessing(null);
        }
        #endregion
    }
}