﻿using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SentenceAnalyser.Models;
using SentenceAnalyser.Services.Abstracts;
using SentenceAnalyser.Utils.Extensions;
using SentenceAnalyser.Utils.Filters;

namespace SentenceAnalyser.Controllers
{
    [Route("api/[controller]")]
    [ValidateModel]
    public class AnalyzerController : Controller
    {
        private readonly IServiceAnalyzer _analyser;
        private readonly ILogger _logger;

        public AnalyzerController(IServiceAnalyzer analyser, ILoggerFactory loggerFactory)
        {
            _analyser = analyser;
            _logger = loggerFactory.CreateLogger<AnalyzerController>();
        }

        // GET api/analyser
        [HttpGet("info")]
        public IActionResult Get()
        {
            return new RedirectResult("~/swagger/");
        }

        // POST api/analyser
        [HttpPost("analyze")]
        public IActionResult AnalyzeData([FromBody] AnalyzerRequest data)
        {
            try
            {
                if (!_analyser.ValidateSentence(data.Sentence))
                {
                    ModelState.AddModelError("error", "Current sentence has invalid characters.");
                    return BadRequest(ModelState);
                }
                AnalyzerResponse response = _analyser.DataProcessing(data);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex,ex.Message);
                ModelState.AddModelError("Exception",ex.Message);
                return BadRequest(ModelState);
            }
        }
    }
}
