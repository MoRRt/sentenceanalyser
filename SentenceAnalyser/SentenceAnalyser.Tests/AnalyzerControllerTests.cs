﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SentenceAnalyser.Controllers;
using SentenceAnalyser.Models;
using SentenceAnalyser.Services;

namespace SentenceAnalyser.Tests
{
    [TestClass]
    public class AnalyzerControllerTests
    {
        private const string InvalidSentence =
            "To package an application to run in Docker, you write a small script called a Dockerfile that~_* automates all+= the steps for deploying the app&.";
        private const string ValidSentence =
            "To package an application to run in Docker, you write a small script called a Dockerfile that automates all the steps for deploying the app.";
        private static readonly int[] LengthsTest = { 5, 8, 2, 6, 7 };

        [TestMethod]
        public void AnalyzeData_CanReturnBadRequestWhenInvalidData()
        {
            AnalyzerController controller = new AnalyzerController(new ServiceAnalyzer(), new LoggerFactory());
            AnalyzerRequest testRequest = new AnalyzerRequest(){Sentence = InvalidSentence,Lengths = LengthsTest};

            IActionResult result = controller.AnalyzeData(testRequest);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }


        [TestMethod]
        public void AnalyzeData_CanReturnOKWhenInvalidData()
        {
            AnalyzerController controller = new AnalyzerController(new ServiceAnalyzer(), new LoggerFactory());
            AnalyzerRequest testRequest = new AnalyzerRequest() { Sentence = ValidSentence, Lengths = LengthsTest };

            IActionResult result = controller.AnalyzeData(testRequest);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
    }
}