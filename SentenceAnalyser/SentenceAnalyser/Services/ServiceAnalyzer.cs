﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using SentenceAnalyser.Models;
using SentenceAnalyser.Services.Abstracts;

namespace SentenceAnalyser.Services
{
    /// <summary>
    /// Class for analyzing sentences
    /// </summary>
    public class ServiceAnalyzer : IServiceAnalyzer
    {
        private const string SeparatedRegularExpression = @"[-.?!)(';,]";
        private const string ValidatedRegularExpression = @"[]_=:&=~`\+*/|\\}{%^$#[]";

        /// <summary>
        /// Method for handle input data
        /// </summary>
        /// <param name="data">Data of input request</param>
        /// <returns>Processed response</returns>
        public AnalyzerResponse DataProcessing(AnalyzerRequest data)
        {
            if (data == null)
                throw new NullReferenceException("Analyzed data hasn't been found.");
            AnalyzerResponse response = new AnalyzerResponse();
            string[] words = Regex.Replace(data.Sentence, SeparatedRegularExpression, "").Split(' ');
            foreach (var length in data.Lengths)
            {
                string[] validatedWords = words.Where(s => s.Length == length).Distinct().ToArray();
                response.Result.Add(new DataInfo
                {
                    Length = length,
                    Count = validatedWords.Length,
                    Words = validatedWords
                });
            }
            return response;
        }

        /// <summary>
        /// Method for validate input sentence if it has invalid characters 
        /// </summary>
        /// <param name="sentence">Input sentence</param>
        /// <returns>Result of validating</returns>
        public bool ValidateSentence(string sentence)
        {
            return !Regex.IsMatch(sentence, ValidatedRegularExpression);
        }
    }
}