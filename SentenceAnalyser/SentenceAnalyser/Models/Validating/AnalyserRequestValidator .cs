﻿namespace SequenceAnalyser.Models.Validating
{
    public class AnalyserRequestValidator : AbstractValidator<AnalyserRequest>
    {
        public AnalyserRequestValidator()
        {
            RuleFor(user => user.UserName).NotEmpty().WithMessage("Username cannot be empty");
            RuleFor(user => user.Password).NotEmpty().WithMessage("Password cannot be empty");
        }
    }
}