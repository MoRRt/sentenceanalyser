﻿using Microsoft.AspNetCore.Mvc.Filters;
using SentenceAnalyser.Models.Validating;

namespace SentenceAnalyser.Utils.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new ValidationFailedResult(context.ModelState);
            }
        }
    }
}