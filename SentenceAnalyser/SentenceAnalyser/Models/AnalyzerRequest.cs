﻿using System.ComponentModel.DataAnnotations;
using SentenceAnalyser.Utils.Filters;

namespace SentenceAnalyser.Models
{
    public class AnalyzerRequest
    {
        [Required(ErrorMessage = "Field 'Sentence' cannot be empty.", AllowEmptyStrings = false)]
        public string Sentence { get; set; }
        [IntegerArrayRequired(ErrorMessage = "Field 'Lengths' cannot be empty.")]
        public int[] Lengths { get; set; }
      
    }
}